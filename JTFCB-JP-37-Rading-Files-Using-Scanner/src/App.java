import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class App {

	public static void main(String[] args) throws FileNotFoundException {

		// mozna czytac ze sciezki
		// trzeba dodac kolejny/ lub pojedynczy obrocony o tak /
		//String fileName = "C:\\Users\\Pawel\\Desktop\\example.txt";
		//mozna przeciagnac do eclipse i zmienic zapis jak ponizej
		String fileName = "example.txt";
		
		//to musimy zaimportowac
		// to jest typ z biblioteki
		File textFile = new File(fileName);
		
		// jest tez file reader czy cos ale on poleca skaner
		// ten throws jest w main method bo tu jest blad pojawiajacy sie
		// ten blad polega na takim zabezpieczeniu jakby nie bylo pliku
		Scanner in = new Scanner(textFile);
		
		// to jest do czytania liczby czyli tej trojki na poczatku
		int value = in.nextInt();
		System.out.println("Read value: " + value);
		
		// to jest zeby nie bylo problemu z line feed
		in.nextLine();
		
		int count = 2;
		
		// petla do czytania
		// dopoki jest linia do czytania
		while (in.hasNextLine()) {
			
			String line = in.nextLine();
			
			System.out.println(count + ": " + line);
			count++;
		}
		// zamykanie skanera
		// trzeba zamykac bo jest blad
		in.close();
	
	}

}
